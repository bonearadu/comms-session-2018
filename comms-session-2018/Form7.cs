﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018 {
	public partial class form_putere : Form {
		public form_putere() {
			InitializeComponent();
		}

        private void button_cont_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "csv files|*.csv";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                Function_Generator fg = new Function_Generator();
                TextBox txt = new TextBox();
                txt.Text = ff.FileName;

                fg.generateFunctionPutere(Convert.ToDouble(textBox_expo.Text), txt);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

		private void button_inapoi_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
