﻿using System;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// Această clasă stă la baza creeri funcțiilor.
    /// Aici se află algoritmi de obținere a fisierelor csv.
    /// </summary>
    class Function_Generator
    {
        double x, y;
		public void generateFunction(int[] coef, int grd, TextBox textBox6) {
			TextWriter txt = new StreamWriter(textBox6.Text);
			txt.WriteLine("x,y");

			for (double i = -100; i <= 100; i += 0.1) {
				x = i;
				y = 0;
				for (int j = 0; j <= grd; j++) {
					if (j == 0) {
						y += coef[j];
					}
					else {
						y += Math.Pow(x, j) * coef[j];
					}
				}
				if (y >= -100 && y <= 100) txt.WriteLine(x + "," + y);
			}
			txt.Close();
		}
        public void generateFunctionRoot(double ordin, TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

            for (double i = -10; i <= 10; i += 0.01)
            {
                x = i;
				if (ordin % 2 == 1) {
					if (x < 0) {
						x *= -1;
						y = Math.Pow(x, 1.0 / ordin) * -1;
						x *= -1;
					}
					else y = Math.Pow(x, 1.0 / ordin);
				}
                else y = Math.Pow(x, 1.0 / ordin);
				if (y >= -100 && y <= 100) txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
        public void generateFunctionPutere(double exponent, TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

            for (double i = -100; i <= 100; i += 0.1)
            {
                x = i;
                y = Math.Pow(x, exponent);
				if (y >= -100 && y <= 100) txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
        public void generateFunctionSine(TextBox textBox6)
	    {
		    TextWriter txt = new StreamWriter(textBox6.Text);
		    txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30)
			{
				x = i + 0.16;
				y =Math.Sin(x);
			    txt.WriteLine(x + "," + y);
		    }   
		    txt.Close();
	    }
        public void generateFunctionCosine(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30) {
				x = i + 0.16;
				y =Math.Cos(x);
                txt.WriteLine(x + "," + y);
            }

            txt.Close();
        }
        public void generateFunctionTangent(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 300) {
				x = i + 0.16;
				y = Math.Tan(x);
				if (y >= -50 && y <= 50) txt.WriteLine(x + "," + y);
			}

			txt.Close();
        }
        public void generateFunctionCotanget(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 300)
            {
				x = i + 0.16;
				y = 1.0 / Math.Tan(x);
				if (y >= -50 && y <= 50) txt.WriteLine(x + "," + y);
			}

            txt.Close();
        }
        public void generateFunctionLogarithmic(double baza, TextBox textBox6)
        {
            if (baza != 1 && baza > 0)
            {
                TextWriter txt = new StreamWriter(textBox6.Text);
                txt.WriteLine("x,y");

				for (double i = 0; i <= 100; i += 0.001)
				{
					x = i;
					y = Math.Log(i, baza);
					if (y >= -100 && y <= 100) txt.WriteLine(x + "," + y);
				}

				txt.Close();
            }
            else MessageBox.Show("Introdu o baza diferita de 1!");
        }
        public void generateFunctionExponantial(double baza, TextBox textBox6)
        {
            if (baza > 0)
            {
                TextWriter txt = new StreamWriter(textBox6.Text);
                txt.WriteLine("x,y");

                for (double i = -10; i <= 10; i += 0.1)
                {
                    x = i;
                    y = Math.Pow(baza, i);
					if (y >= -100 && y <= 100) txt.WriteLine(x + "," + y);
                }

                txt.Close();
            }
            else MessageBox.Show("Introdu o baza mai mare decat 0!");
        }
        public void generateFunctionAsin(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30) {
				x = i + 0.16;
				y = Math.Asin(x);
                txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
        public void generateFunctionAcos(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30) {
				x = i + 0.16;
				y = Math.Acos(x);
                txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
        public void generateFunctionAtg(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30) {
				x = i + 0.16;
				y = Math.Atan(x);
                txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
        public void generateFunctionActg(TextBox textBox6)
        {
            TextWriter txt = new StreamWriter(textBox6.Text);
            txt.WriteLine("x,y");

			for (double i = Math.PI * -3; i <= Math.PI * 3; i += Math.PI / 30) {
                x = i + 0.16;
                y = Math.Atan(1.0 / x);
                txt.WriteLine(x + "," + y);
            }
            txt.Close();
        }
    }
}
