﻿using System.IO;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// Clasă destinată citirii și interpretării fișierelor *.csv.
    /// </summary>
    public class Citire
    {
        public string[] header;
        private float[,] data;
        private int nLines;
        private int nColumns;
        public Citire(string myStream)
        {
            string aux;
            string[] pieces;

            StreamReader sr = new StreamReader(myStream);
            aux = sr.ReadLine();
            header = aux.Split(',');
            nColumns = header.Length;
            nLines = 0;
            while ((aux = sr.ReadLine()) != null)
            {
                if (aux.Length > 0) nLines++;
            }

            data = new float[nLines, nColumns];
            sr.BaseStream.Seek(0, 0);
            sr.ReadLine();
            for (int i = 1; i < nLines; i++)
            {
                aux = sr.ReadLine();
                pieces = aux.Split(',');
                for (int j = 0; j < nColumns; j++)
                {
                    data[i, j] = float.Parse(pieces[j]);
                }
            }
            sr.Close();
        }

        public int getLines()
        {
            return this.nLines;
        }
        public int getCols()
        {
            return this.nColumns;
        }
        public float[,] getData()
        {
            return this.data;
        }
        public string[] getHeader()
        {
            return this.header;
        }
    }
}
