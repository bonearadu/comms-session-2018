﻿using System;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018
{
    /// <summary>
    /// Acest form este dedicat creeri funcțiilor logaritmice
    /// Pentru a creea funcția se introduc datele în casetele corespunzătoare și se apasă pe butonul "Continuă"
    /// </summary>
    public partial class form_loga : Form
    {
        public form_loga()
        {
            InitializeComponent();
        }

        private void button_cont_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "csv files|*.csv";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                Function_Generator fg = new Function_Generator();
                TextBox txt = new TextBox();
                txt.Text = ff.FileName;

                fg.generateFunctionLogarithmic(Convert.ToDouble(textBox_baza.Text), txt);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private void button_anulare_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void form_loga_Load(object sender, EventArgs e)
        {

        }

		private void textBox_baza_TextChanged(object sender, EventArgs e) {

		}

		private void button_inapoi_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
