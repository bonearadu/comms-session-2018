﻿namespace comms_session_2018 {
	partial class form_chooseFunction {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_chooseFunction));
			this.label_title = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label_funcionText = new System.Windows.Forms.Label();
			this.button_next = new System.Windows.Forms.Button();
			this.button_cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(16, 11);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(287, 31);
			this.label_title.TabIndex = 0;
			this.label_title.Text = "1. Alege tipul funcției";
			// 
			// comboBox1
			// 
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
            "Polinomială",
            "Trigonometrică",
            "Logaritmică",
            "Exponențială",
            "Putere",
            "Radical"});
			this.comboBox1.Location = new System.Drawing.Point(108, 81);
			this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(220, 37);
			this.comboBox1.TabIndex = 1;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label_funcionText
			// 
			this.label_funcionText.AutoSize = true;
			this.label_funcionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_funcionText.Location = new System.Drawing.Point(17, 89);
			this.label_funcionText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_funcionText.Name = "label_funcionText";
			this.label_funcionText.Size = new System.Drawing.Size(82, 25);
			this.label_funcionText.TabIndex = 2;
			this.label_funcionText.Text = "Funcție:";
			// 
			// button_next
			// 
			this.button_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_next.Location = new System.Drawing.Point(183, 167);
			this.button_next.Margin = new System.Windows.Forms.Padding(4);
			this.button_next.Name = "button_next";
			this.button_next.Size = new System.Drawing.Size(147, 86);
			this.button_next.TabIndex = 3;
			this.button_next.Text = "Continuare";
			this.button_next.UseVisualStyleBackColor = true;
			this.button_next.Click += new System.EventHandler(this.button_next_Click);
			// 
			// button_cancel
			// 
			this.button_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_cancel.Location = new System.Drawing.Point(23, 167);
			this.button_cancel.Margin = new System.Windows.Forms.Padding(4);
			this.button_cancel.Name = "button_cancel";
			this.button_cancel.Size = new System.Drawing.Size(147, 86);
			this.button_cancel.TabIndex = 5;
			this.button_cancel.Text = "Ieșire";
			this.button_cancel.UseVisualStyleBackColor = true;
			this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
			// 
			// form_chooseFunction
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(348, 268);
			this.Controls.Add(this.button_cancel);
			this.Controls.Add(this.button_next);
			this.Controls.Add(this.label_funcionText);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "form_chooseFunction";
			this.Text = "Function Plotter";
			this.Load += new System.EventHandler(this.form_chooseFunction_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label_title;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label_funcionText;
		private System.Windows.Forms.Button button_next;
		private System.Windows.Forms.Button button_cancel;
	}
}