﻿namespace comms_session_2018 {
	partial class form_poly {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_poly));
			this.label_title = new System.Windows.Forms.Label();
			this.label_grade = new System.Windows.Forms.Label();
			this.label_coefVal = new System.Windows.Forms.Label();
			this.textBox_grade = new System.Windows.Forms.TextBox();
			this.textBox_coefVal = new System.Windows.Forms.TextBox();
			this.label_info1 = new System.Windows.Forms.Label();
			this.button_back = new System.Windows.Forms.Button();
			this.button_next = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(17, 16);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(294, 31);
			this.label_title.TabIndex = 0;
			this.label_title.Text = "II. Parametrii Funcției";
			// 
			// label_grade
			// 
			this.label_grade.AutoSize = true;
			this.label_grade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_grade.Location = new System.Drawing.Point(131, 75);
			this.label_grade.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_grade.Name = "label_grade";
			this.label_grade.Size = new System.Drawing.Size(55, 25);
			this.label_grade.TabIndex = 1;
			this.label_grade.Text = "Grad";
			// 
			// label_coefVal
			// 
			this.label_coefVal.AutoSize = true;
			this.label_coefVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_coefVal.Location = new System.Drawing.Point(25, 122);
			this.label_coefVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_coefVal.Name = "label_coefVal";
			this.label_coefVal.Size = new System.Drawing.Size(153, 25);
			this.label_coefVal.TabIndex = 2;
			this.label_coefVal.Text = "Valori coeficienți";
			// 
			// textBox_grade
			// 
			this.textBox_grade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox_grade.Location = new System.Drawing.Point(199, 71);
			this.textBox_grade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.textBox_grade.Name = "textBox_grade";
			this.textBox_grade.Size = new System.Drawing.Size(271, 30);
			this.textBox_grade.TabIndex = 3;
			// 
			// textBox_coefVal
			// 
			this.textBox_coefVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox_coefVal.Location = new System.Drawing.Point(199, 118);
			this.textBox_coefVal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.textBox_coefVal.Name = "textBox_coefVal";
			this.textBox_coefVal.Size = new System.Drawing.Size(271, 30);
			this.textBox_coefVal.TabIndex = 4;
			// 
			// label_info1
			// 
			this.label_info1.AutoSize = true;
			this.label_info1.Location = new System.Drawing.Point(195, 154);
			this.label_info1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_info1.Name = "label_info1";
			this.label_info1.Size = new System.Drawing.Size(275, 34);
			this.label_info1.TabIndex = 5;
			this.label_info1.Text = "Ordine descrescătoare, în funcție de grad.\r\nSeparate prin virgulă.\r\n";
			// 
			// button_back
			// 
			this.button_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_back.Location = new System.Drawing.Point(16, 252);
			this.button_back.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_back.Name = "button_back";
			this.button_back.Size = new System.Drawing.Size(212, 86);
			this.button_back.TabIndex = 7;
			this.button_back.Text = "Ieșire";
			this.button_back.UseVisualStyleBackColor = true;
			this.button_back.Click += new System.EventHandler(this.button_back_Click);
			// 
			// button_next
			// 
			this.button_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_next.Location = new System.Drawing.Point(257, 252);
			this.button_next.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_next.Name = "button_next";
			this.button_next.Size = new System.Drawing.Size(212, 86);
			this.button_next.TabIndex = 6;
			this.button_next.Text = "Continuare";
			this.button_next.UseVisualStyleBackColor = true;
			this.button_next.Click += new System.EventHandler(this.button_next_Click);
			// 
			// form_poly
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(485, 353);
			this.Controls.Add(this.button_back);
			this.Controls.Add(this.button_next);
			this.Controls.Add(this.label_info1);
			this.Controls.Add(this.textBox_coefVal);
			this.Controls.Add(this.textBox_grade);
			this.Controls.Add(this.label_coefVal);
			this.Controls.Add(this.label_grade);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "form_poly";
			this.Text = "Function Plotter";
			this.Load += new System.EventHandler(this.form_poly_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label_title;
		private System.Windows.Forms.Label label_grade;
		private System.Windows.Forms.Label label_coefVal;
		private System.Windows.Forms.TextBox textBox_grade;
		private System.Windows.Forms.TextBox textBox_coefVal;
		private System.Windows.Forms.Label label_info1;
		private System.Windows.Forms.Button button_back;
		private System.Windows.Forms.Button button_next;
	}
}