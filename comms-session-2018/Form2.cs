﻿using System;
using System.Windows.Forms;

namespace comms_session_2018 {
    /// <summary>
    /// Acest form este dedicat primului pas în creerea unei funcții, acela fiind selectarea tipului acestuia.
    /// </summary>
	public partial class form_chooseFunction : Form {
		public form_chooseFunction() {
			InitializeComponent();
		}

        private void form_chooseFunction_Load(object sender, EventArgs e)
        {

        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_next_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Equals("Polinomială"))
            {
                form_poly polinomial = new form_poly();
                polinomial.ShowDialog();
            }
            else if (comboBox1.Text.Equals("Trigonometrică"))
            {
                Form_trigo trigono = new Form_trigo();
                trigono.ShowDialog();
            }
            else if (comboBox1.Text.Equals("Logaritmică"))
            {
                form_loga loga = new form_loga();
                loga.ShowDialog();
            }
            else if (comboBox1.Text.Equals("Exponențială"))
            {
                form_exp expo = new form_exp();
                expo.ShowDialog();
            }
            else if (comboBox1.Text.Equals("Radical"))
            {
                form_rad rad = new form_rad();
                rad.ShowDialog();
            }
            else if (comboBox1.Text.Equals("Putere"))
            {
                form_putere put = new form_putere();
                put.ShowDialog();
            }
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
