﻿namespace comms_session_2018 {
	partial class form_graph {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.label_title = new System.Windows.Forms.Label();
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.chart_graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.button_open = new System.Windows.Forms.Button();
			this.button_plot = new System.Windows.Forms.Button();
			this.button_functionWizard = new System.Windows.Forms.Button();
			this.button_save = new System.Windows.Forms.Button();
			this.button_exit = new System.Windows.Forms.Button();
			this.comboBox_xBox = new System.Windows.Forms.ComboBox();
			this.comboBox_yBox = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart_graph)).BeginInit();
			this.SuspendLayout();
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(19, 11);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(1075, 42);
			this.label_title.TabIndex = 0;
			this.label_title.Text = "Reprezentarea grafică a funcțiilor, utilizând .NET Framework";
			this.label_title.Click += new System.EventHandler(this.label_title_Click);
			// 
			// chart1
			// 
			this.chart1.Location = new System.Drawing.Point(0, 0);
			this.chart1.Name = "chart1";
			this.chart1.Size = new System.Drawing.Size(300, 300);
			this.chart1.TabIndex = 0;
			// 
			// chart2
			// 
			this.chart2.Location = new System.Drawing.Point(0, 0);
			this.chart2.Name = "chart2";
			this.chart2.Size = new System.Drawing.Size(300, 300);
			this.chart2.TabIndex = 0;
			// 
			// chart3
			// 
			this.chart3.Location = new System.Drawing.Point(0, 0);
			this.chart3.Name = "chart3";
			this.chart3.Size = new System.Drawing.Size(300, 300);
			this.chart3.TabIndex = 0;
			// 
			// chart_graph
			// 
			chartArea4.Name = "ChartArea1";
			this.chart_graph.ChartAreas.Add(chartArea4);
			legend4.Name = "Legend1";
			this.chart_graph.Legends.Add(legend4);
			this.chart_graph.Location = new System.Drawing.Point(16, 66);
			this.chart_graph.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.chart_graph.Name = "chart_graph";
			series4.ChartArea = "ChartArea1";
			series4.Legend = "Legend1";
			series4.Name = "Series1";
			this.chart_graph.Series.Add(series4);
			this.chart_graph.Size = new System.Drawing.Size(1147, 511);
			this.chart_graph.TabIndex = 1;
			this.chart_graph.Text = "Graficul Funcției";
			this.chart_graph.Click += new System.EventHandler(this.chart4_Click);
			// 
			// button_open
			// 
			this.button_open.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_open.Location = new System.Drawing.Point(16, 601);
			this.button_open.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_open.Name = "button_open";
			this.button_open.Size = new System.Drawing.Size(160, 75);
			this.button_open.TabIndex = 2;
			this.button_open.Text = "Deschide";
			this.button_open.UseVisualStyleBackColor = true;
			this.button_open.Click += new System.EventHandler(this.button_open_Click);
			// 
			// button_plot
			// 
			this.button_plot.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_plot.Location = new System.Drawing.Point(208, 601);
			this.button_plot.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_plot.Name = "button_plot";
			this.button_plot.Size = new System.Drawing.Size(160, 75);
			this.button_plot.TabIndex = 3;
			this.button_plot.Text = "Desenează";
			this.button_plot.UseVisualStyleBackColor = true;
			this.button_plot.Click += new System.EventHandler(this.button_plot_Click);
			// 
			// button_functionWizard
			// 
			this.button_functionWizard.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_functionWizard.ForeColor = System.Drawing.Color.Red;
			this.button_functionWizard.Location = new System.Drawing.Point(400, 601);
			this.button_functionWizard.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_functionWizard.Name = "button_functionWizard";
			this.button_functionWizard.Size = new System.Drawing.Size(380, 75);
			this.button_functionWizard.TabIndex = 4;
			this.button_functionWizard.Text = "Crează Funcția";
			this.button_functionWizard.UseVisualStyleBackColor = true;
			this.button_functionWizard.Click += new System.EventHandler(this.button_functionWizard_Click);
			// 
			// button_save
			// 
			this.button_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_save.Location = new System.Drawing.Point(811, 601);
			this.button_save.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_save.Name = "button_save";
			this.button_save.Size = new System.Drawing.Size(160, 75);
			this.button_save.TabIndex = 5;
			this.button_save.Text = "Salvează";
			this.button_save.UseVisualStyleBackColor = true;
			this.button_save.Click += new System.EventHandler(this.button_save_Click);
			// 
			// button_exit
			// 
			this.button_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_exit.Location = new System.Drawing.Point(1003, 601);
			this.button_exit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_exit.Name = "button_exit";
			this.button_exit.Size = new System.Drawing.Size(160, 75);
			this.button_exit.TabIndex = 6;
			this.button_exit.Text = "Ieșire";
			this.button_exit.UseVisualStyleBackColor = true;
			this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
			// 
			// comboBox_xBox
			// 
			this.comboBox_xBox.FormattingEnabled = true;
			this.comboBox_xBox.Location = new System.Drawing.Point(17, 66);
			this.comboBox_xBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBox_xBox.Name = "comboBox_xBox";
			this.comboBox_xBox.Size = new System.Drawing.Size(160, 24);
			this.comboBox_xBox.TabIndex = 7;
			this.comboBox_xBox.Visible = false;
			// 
			// comboBox_yBox
			// 
			this.comboBox_yBox.FormattingEnabled = true;
			this.comboBox_yBox.Location = new System.Drawing.Point(17, 100);
			this.comboBox_yBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBox_yBox.Name = "comboBox_yBox";
			this.comboBox_yBox.Size = new System.Drawing.Size(160, 24);
			this.comboBox_yBox.TabIndex = 8;
			this.comboBox_yBox.Visible = false;
			// 
			// form_graph
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(1179, 690);
			this.Controls.Add(this.comboBox_yBox);
			this.Controls.Add(this.comboBox_xBox);
			this.Controls.Add(this.button_exit);
			this.Controls.Add(this.button_save);
			this.Controls.Add(this.button_functionWizard);
			this.Controls.Add(this.button_plot);
			this.Controls.Add(this.button_open);
			this.Controls.Add(this.chart_graph);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "form_graph";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Function Plotter";
			this.Load += new System.EventHandler(this.form_graph_Load);
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart_graph)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label_title;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart_graph;
		private System.Windows.Forms.Button button_open;
		private System.Windows.Forms.Button button_plot;
		private System.Windows.Forms.Button button_functionWizard;
		private System.Windows.Forms.Button button_save;
		private System.Windows.Forms.Button button_exit;
		private System.Windows.Forms.ComboBox comboBox_xBox;
		private System.Windows.Forms.ComboBox comboBox_yBox;
	}
}

