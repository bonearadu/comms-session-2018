﻿using System;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018 {
    /// <summary>
    /// Acest form este dedicat creeri funcțiilor trigonometrice
    /// Pentru a creea funcția se introduc datele în casetele corespunzătoare și se apasă pe butonul "Continuă"
    /// </summary>
    public partial class Form_trigo : Form {
		public Form_trigo() {
			InitializeComponent();
		}

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_next_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "csv files|*.csv";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                Function_Generator fg = new Function_Generator();
                TextBox txt = new TextBox();
                txt.Text = ff.FileName;
                switch (comboBox_function.Text)
                {
                    case "sin":
                        fg.generateFunctionSine(txt);
                        break;
                    case "cos":
                        fg.generateFunctionCosine(txt);
                        break;
                    case "tg":
                        fg.generateFunctionTangent(txt);
                        break;
                    case "ctg":
                        fg.generateFunctionCotanget(txt);
                        break;
                    case "arcsin":
                        fg.generateFunctionAsin(txt);
                        break;
                    case "arccos":
                        fg.generateFunctionAcos(txt);
                        break;
                    case "arctan":
                        fg.generateFunctionAtg(txt);
                        break;
                    case "arcctg":
                        fg.generateFunctionActg(txt);
                        break;
                    default:
                        MessageBox.Show("A aparut o eroare!");
                        break;
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private void Form_trigo_Load(object sender, EventArgs e)
        {

        }

		private void button_back_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
