﻿namespace comms_session_2018 {
	partial class Form_trigo {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_trigo));
			this.button_back = new System.Windows.Forms.Button();
			this.button_next = new System.Windows.Forms.Button();
			this.label_function = new System.Windows.Forms.Label();
			this.label_title = new System.Windows.Forms.Label();
			this.comboBox_function = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// button_back
			// 
			this.button_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_back.Location = new System.Drawing.Point(16, 176);
			this.button_back.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_back.Name = "button_back";
			this.button_back.Size = new System.Drawing.Size(147, 86);
			this.button_back.TabIndex = 16;
			this.button_back.Text = "Ieșire";
			this.button_back.UseVisualStyleBackColor = true;
			this.button_back.Click += new System.EventHandler(this.button_back_Click);
			// 
			// button_next
			// 
			this.button_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_next.Location = new System.Drawing.Point(185, 176);
			this.button_next.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_next.Name = "button_next";
			this.button_next.Size = new System.Drawing.Size(147, 86);
			this.button_next.TabIndex = 15;
			this.button_next.Text = "Continuare";
			this.button_next.UseVisualStyleBackColor = true;
			this.button_next.Click += new System.EventHandler(this.button_next_Click);
			// 
			// label_function
			// 
			this.label_function.AutoSize = true;
			this.label_function.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_function.Location = new System.Drawing.Point(41, 91);
			this.label_function.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_function.Name = "label_function";
			this.label_function.Size = new System.Drawing.Size(76, 25);
			this.label_function.TabIndex = 10;
			this.label_function.Text = "Funcție";
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(17, 16);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(294, 31);
			this.label_title.TabIndex = 9;
			this.label_title.Text = "II. Parametrii Funcției";
			// 
			// comboBox_function
			// 
			this.comboBox_function.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBox_function.FormattingEnabled = true;
			this.comboBox_function.Items.AddRange(new object[] {
            "sin",
            "cos",
            "tg",
            "ctg",
            "arcsin",
            "arccos",
            "arctan",
            "arcctg"});
			this.comboBox_function.Location = new System.Drawing.Point(132, 87);
			this.comboBox_function.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.comboBox_function.Name = "comboBox_function";
			this.comboBox_function.Size = new System.Drawing.Size(160, 33);
			this.comboBox_function.TabIndex = 18;
			// 
			// Form_trigo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(347, 277);
			this.Controls.Add(this.comboBox_function);
			this.Controls.Add(this.button_back);
			this.Controls.Add(this.button_next);
			this.Controls.Add(this.label_function);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "Form_trigo";
			this.Text = "Function Plotter";
			this.Load += new System.EventHandler(this.Form_trigo_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button button_back;
		private System.Windows.Forms.Button button_next;
		private System.Windows.Forms.Label label_function;
		private System.Windows.Forms.Label label_title;
		private System.Windows.Forms.ComboBox comboBox_function;
	}
}