﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018 {
    /// <summary>
    /// Form-ul principal.
    /// În acest form se reprezintă grafic funcțiile.
    /// De asemenea aici se pot salva reprezentăriile grafice în format .jpeg
    /// </summary>
	public partial class form_graph : Form {
		public form_graph() {
			InitializeComponent();
		}

		private void label_title_Click(object sender, EventArgs e) {

		}

		private void chart4_Click(object sender, EventArgs e) {

		}

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        List<Citire> rrList = new List<Citire>();
        Citire rr;
        private void button_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ff = new OpenFileDialog();


            ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            ff.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            ff.Multiselect = true;
            ff.FilterIndex = 1;
            ff.RestoreDirectory = true;

            if (ff.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    rrList.Clear();
                    foreach (String file in ff.FileNames)
                    {
                        rr = new Citire(file);
                        rrList.Add(rr);
                    }

                    if (rrList.Count > 0)
                    {
                        string[] header = rrList[0].header;
                        comboBox_xBox.DataSource = header;
                        comboBox_yBox.DataSource = header.Clone();
                    }
                    if (comboBox_yBox.Items.Count > 1) comboBox_yBox.SelectedIndex = 1;
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
        }

        private void button_plot_Click(object sender, EventArgs e)
        {
            if (rr != null)
            {
                Plot pl = new WindowsFormsApplication1.Plot(rr, comboBox_xBox, comboBox_yBox, chart_graph);
            }
            else
            {
                MessageBox.Show("Error, no data to plot! Please load csv file!");
                return;
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "jpg files|*.jpg";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                chart_graph.SaveImage(ff.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch(Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private void button_functionWizard_Click(object sender, EventArgs e)
        {
            form_chooseFunction creare = new form_chooseFunction();
            creare.ShowDialog();
        }

        private void form_graph_Load(object sender, EventArgs e)
        {

        }
    }
}
