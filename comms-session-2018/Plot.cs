﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// Această clasă este responsabilă pentru creerea desenului.
    /// Ea folosește un obiect din biblioteca System.Windows.Forms.DataVisualization pentru a reprezenta grafic într-un mod ușor de înteles și citit.
    /// </summary>
    public class Plot
    {
        public Plot(Citire rr, ComboBox xBox, ComboBox yBox, Chart chart)
        {
            int indX = xBox.SelectedIndex;
            int indY = yBox.SelectedIndex;
            float[,] data = rr.getData();
            int nLines = rr.getLines();
            int nColumns = rr.getCols();
            string[] header = rr.getHeader();
            
            chart.Series.Clear();
            chart.Series.Add("Series0");
            chart.Series[0].Points.Clear();
            chart.Series[0].ChartType = SeriesChartType.Line;
            chart.ChartAreas[0].AxisX.LabelStyle.Format = "{F2}";
            chart.ChartAreas[0].AxisX.Title = header[indX];
            chart.ChartAreas[0].AxisY.Title = header[indY];
            chart.Legends.Clear();
            float xGrids = 6;
            float[] ext = getExtrema(data, nLines, nColumns, indX);
            chart.ChartAreas[0].AxisX.MajorGrid.Interval = (ext[1] - ext[0]) / xGrids;
            chart.ChartAreas[0].AxisX.LabelStyle.Interval = (ext[1] - ext[0]) / xGrids;
            chart.ChartAreas[0].AxisX.MajorTickMark.Interval = (ext[1] - ext[0]) / xGrids;
            for (int j = 0; j < nLines; j++)
            {
                chart.Series[0].Points.AddXY(data[j, indX], data[j, indY]);
            }
            chart.Series[0].Points.RemoveAt(0);
        }
        private float[] getExtrema(float[,] data,int nL,int nC,int idx)
        {
            float min = data[0, idx], max = data[0, idx];
            float[] res = new float[2];
            for (int i=1;i<nL;i++)
            {
                if (min > data[i, idx]) min = data[i, idx];
                if (max < data[i, idx]) max = data[i, idx];
            }
            res[0] = min;
            res[1] = max;
            return res;
        }
    }
}
