﻿namespace comms_session_2018 {
	partial class form_exp {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_exp));
			this.button_back = new System.Windows.Forms.Button();
			this.button_next = new System.Windows.Forms.Button();
			this.label_base = new System.Windows.Forms.Label();
			this.label_title = new System.Windows.Forms.Label();
			this.textBox_base = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button_back
			// 
			this.button_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_back.Location = new System.Drawing.Point(15, 171);
			this.button_back.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_back.Name = "button_back";
			this.button_back.Size = new System.Drawing.Size(148, 86);
			this.button_back.TabIndex = 22;
			this.button_back.Text = "Ieșire";
			this.button_back.UseVisualStyleBackColor = true;
			this.button_back.Click += new System.EventHandler(this.button_back_Click);
			// 
			// button_next
			// 
			this.button_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_next.Location = new System.Drawing.Point(183, 171);
			this.button_next.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.button_next.Name = "button_next";
			this.button_next.Size = new System.Drawing.Size(148, 86);
			this.button_next.TabIndex = 21;
			this.button_next.Text = "Continuare";
			this.button_next.UseVisualStyleBackColor = true;
			this.button_next.Click += new System.EventHandler(this.button_next_Click);
			// 
			// label_base
			// 
			this.label_base.AutoSize = true;
			this.label_base.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_base.Location = new System.Drawing.Point(89, 92);
			this.label_base.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_base.Name = "label_base";
			this.label_base.Size = new System.Drawing.Size(57, 25);
			this.label_base.TabIndex = 20;
			this.label_base.Text = "Bază";
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(16, 11);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(294, 31);
			this.label_title.TabIndex = 19;
			this.label_title.Text = "II. Parametrii Funcției";
			// 
			// textBox_base
			// 
			this.textBox_base.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox_base.Location = new System.Drawing.Point(159, 89);
			this.textBox_base.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.textBox_base.Name = "textBox_base";
			this.textBox_base.Size = new System.Drawing.Size(83, 30);
			this.textBox_base.TabIndex = 24;
			this.textBox_base.TextChanged += new System.EventHandler(this.textBox_base_TextChanged);
			// 
			// form_exp
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(348, 277);
			this.Controls.Add(this.textBox_base);
			this.Controls.Add(this.button_back);
			this.Controls.Add(this.button_next);
			this.Controls.Add(this.label_base);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "form_exp";
			this.Text = "Function Plotter";
			this.Load += new System.EventHandler(this.form_exp_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button button_back;
		private System.Windows.Forms.Button button_next;
		private System.Windows.Forms.Label label_base;
		private System.Windows.Forms.Label label_title;
		private System.Windows.Forms.TextBox textBox_base;
	}
}