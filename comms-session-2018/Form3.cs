﻿using System;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018 { 
    /// <summary>
    /// Acest form este dedicat creeri funcțiilor polinomiale
    /// Pentru a creea funcția se introduc datele în casetele corespunzătoare și se apasă pe butonul "Continuă"
    /// </summary>
	public partial class form_poly : Form {
		public form_poly() {
			InitializeComponent();
		}

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private int[] coefParse(string coef)
        {
            int[] a = new int[20];

            string[] coefParsed = coef.Split(',');
            for (int i = 0; i < coefParsed.Length; i++)
            {
                try
                {
                    a[i] = Convert.ToInt32(coefParsed[i]);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Date de intrare eronate!");
                    Console.WriteLine(e.Message);
                }
            }
            return a;
        }
        private void button_next_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "csv files|*.csv";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                Function_Generator fg = new Function_Generator();
                TextBox txt = new TextBox();
                txt.Text = ff.FileName;
                fg.generateFunction(coefParse(textBox_coefVal.Text), Convert.ToInt32(textBox_grade.Text), txt);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private void form_poly_Load(object sender, EventArgs e)
        {

        }

		private void button_back_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
