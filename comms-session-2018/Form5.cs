﻿using System;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace comms_session_2018 {
    /// <summary>
    /// Acest form este dedicat creeri funcțiilor exponențiale
    /// Pentru a creea funcția se introduc datele în casetele corespunzătoare și se apasă pe butonul "Continuă"
    /// </summary>
    public partial class form_exp : Form {
		public form_exp() {
			InitializeComponent();
		}

        private void button_next_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ff = new SaveFileDialog();

                ff.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ff.Filter = "csv files|*.csv";
                ff.FilterIndex = 1;
                ff.RestoreDirectory = true;
                ff.ShowDialog();

                Function_Generator fg = new Function_Generator();
                TextBox txt = new TextBox();
                txt.Text = ff.FileName;

                fg.generateFunctionExponantial(Convert.ToDouble(textBox_base.Text), txt);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        private void button_back_Click(object sender, EventArgs e)
        {
			this.Close();
		}

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void form_exp_Load(object sender, EventArgs e)
        {

        }

		private void textBox_base_TextChanged(object sender, EventArgs e) {

		}
	}
}
