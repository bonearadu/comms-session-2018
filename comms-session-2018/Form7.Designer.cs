﻿namespace comms_session_2018 {
	partial class form_putere {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_putere));
			this.textBox_expo = new System.Windows.Forms.TextBox();
			this.button_inapoi = new System.Windows.Forms.Button();
			this.button_cont = new System.Windows.Forms.Button();
			this.label_expo = new System.Windows.Forms.Label();
			this.label_title = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// textBox_expo
			// 
			this.textBox_expo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox_expo.Location = new System.Drawing.Point(184, 94);
			this.textBox_expo.Margin = new System.Windows.Forms.Padding(4);
			this.textBox_expo.Name = "textBox_expo";
			this.textBox_expo.Size = new System.Drawing.Size(83, 30);
			this.textBox_expo.TabIndex = 35;
			// 
			// button_inapoi
			// 
			this.button_inapoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_inapoi.Location = new System.Drawing.Point(16, 176);
			this.button_inapoi.Margin = new System.Windows.Forms.Padding(4);
			this.button_inapoi.Name = "button_inapoi";
			this.button_inapoi.Size = new System.Drawing.Size(148, 86);
			this.button_inapoi.TabIndex = 34;
			this.button_inapoi.Text = "Ieșire";
			this.button_inapoi.UseVisualStyleBackColor = true;
			this.button_inapoi.Click += new System.EventHandler(this.button_inapoi_Click);
			// 
			// button_cont
			// 
			this.button_cont.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button_cont.Location = new System.Drawing.Point(184, 176);
			this.button_cont.Margin = new System.Windows.Forms.Padding(4);
			this.button_cont.Name = "button_cont";
			this.button_cont.Size = new System.Drawing.Size(148, 86);
			this.button_cont.TabIndex = 33;
			this.button_cont.Text = "Continuare";
			this.button_cont.UseVisualStyleBackColor = true;
			this.button_cont.Click += new System.EventHandler(this.button_cont_Click);
			// 
			// label_expo
			// 
			this.label_expo.AutoSize = true;
			this.label_expo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_expo.Location = new System.Drawing.Point(73, 97);
			this.label_expo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_expo.Name = "label_expo";
			this.label_expo.Size = new System.Drawing.Size(95, 25);
			this.label_expo.TabIndex = 32;
			this.label_expo.Text = "Exponent";
			// 
			// label_title
			// 
			this.label_title.AutoSize = true;
			this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_title.Location = new System.Drawing.Point(17, 16);
			this.label_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label_title.Name = "label_title";
			this.label_title.Size = new System.Drawing.Size(294, 31);
			this.label_title.TabIndex = 31;
			this.label_title.Text = "II. Parametrii Funcției";
			// 
			// form_putere
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(348, 277);
			this.Controls.Add(this.textBox_expo);
			this.Controls.Add(this.button_inapoi);
			this.Controls.Add(this.button_cont);
			this.Controls.Add(this.label_expo);
			this.Controls.Add(this.label_title);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "form_putere";
			this.Text = "Function Plotter";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBox_expo;
		private System.Windows.Forms.Button button_inapoi;
		private System.Windows.Forms.Button button_cont;
		private System.Windows.Forms.Label label_expo;
		private System.Windows.Forms.Label label_title;
	}
}